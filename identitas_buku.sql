-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2021 at 08:01 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `destian`
--

-- --------------------------------------------------------

--
-- Table structure for table `beli`
--

CREATE TABLE `beli` (
  `beli_id` varchar(50) NOT NULL,
  `beli_bukuID` varchar(50) NOT NULL,
  `beli_jumlah` varchar(50) NOT NULL,
  `beli_waktu` varchar(50) NOT NULL,
  `beli_userID` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `beli`
--

INSERT INTO `beli` (`beli_id`, `beli_bukuID`, `beli_jumlah`, `beli_waktu`, `beli_userID`) VALUES
('1', 'a001', '2', '12.51', 'user1');

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `buku_id` varchar(50) NOT NULL,
  `buku_judul` varchar(50) NOT NULL,
  `buku_pengarang` varchar(50) NOT NULL,
  `buku_tahunTerbit` varchar(50) NOT NULL,
  `buku_penerbit` varchar(50) NOT NULL,
  `buku_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`buku_id`, `buku_judul`, `buku_pengarang`, `buku_tahunTerbit`, `buku_penerbit`, `buku_harga`) VALUES
('a001', 'The Power of Six', 'Manusia', '2015', 'Erlangga', 100000),
('a002', 'Harry Potter Deadly Hollow', 'bukan aku', '2014', 'Erlangga', 20000),
('a003', 'Firebelly', 'Ntah siapa', '2007', 'Erlangga', 15000),
('b001', 'Masamune Revenge', 'Wibu', '2013', 'Akasia', 23000),
('b002', 'Marvel', 'Wibu', '1945', 'Disney', 25000),
('b003', 'Matematika dasar', 'dosen ..', '2012', 'UAD', 15000),
('c001', 'test', 'test', 'sets', 'test', 1111);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` varchar(50) NOT NULL,
  `user_usrname` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_nama` varchar(50) NOT NULL,
  `user_alamat` varchar(50) NOT NULL,
  `user_noHP` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_usrname`, `user_password`, `user_nama`, `user_alamat`, `user_noHP`) VALUES
('user1', 'rend', 'ray', 'test', 'indonesia', '190827563');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beli`
--
ALTER TABLE `beli`
  ADD PRIMARY KEY (`beli_id`),
  ADD KEY `beli_userID` (`beli_userID`),
  ADD KEY `beli_bukuID` (`beli_bukuID`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`buku_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
