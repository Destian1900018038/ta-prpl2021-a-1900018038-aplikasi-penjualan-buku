<?php
include ('config.php');

if (!isset($_SESSION['loginUser'])) {
    header('Location: login.php');
}

$user_nama = $_SESSION['loginUser'];
$user_alamat = $_SESSION['loginAlamat'];
$user_nohp = $_SESSION['loginNohp'];
$user_id = $_SESSION['iduser'];

$buku = $_GET['bukuid'];
//var_dump($buku);
//die;
$a = 0;
$pesan_sql=mysqli_query($con,"SELECT * FROM buku WHERE buku_id = '$buku'");
while($row3=mysqli_fetch_assoc($pesan_sql)){
    $jud = $row3['buku_judul'];
    $har = $row3['buku_harga'];
}

$sql_check = mysqli_query($con,"SELECT MAX(beli_id) AS id FROM beli");
$row = mysqli_fetch_array($sql_check, MYSQLI_ASSOC);
$beli_id = (int)$row['id'];
if (is_null($beli_id)){
    $beli_id = 1;
} else {
    $beli_id++;
}

$date= date("Y-m-d");
$time=date("H:m");
$dd=$date." ".$time;

if (isset($_POST['beli'])) {
    $jml = $_POST['jumlah_buku'];
    $hrg = $_POST['harga'];
    $atot = (int)$jml * (int)$hrg;
    
    $sql_tambah = "INSERT INTO beli VALUES ('$beli_id', '$buku', '$jml', '$dd', '$hrg', '$user_id')";
    mysqli_query($con, $sql_tambah);

    $a = 1;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Transaksi</title>
</head>
<body>
    <h1>FORM TRANSAKSI</h1>
    <form method="post"> 
        <table>
            <tr>
                <td>Judul Buku</td>
                <td><input id="jb" type="text" name="judul_buku" value="<?=$jud;?>"></td>
            </tr>
            <tr>
                <td>Jumlah Buku</td>
                <td><input id="jumlah" type="number" name="jumlah_buku"></td>
            </tr>
            <tr>
                <td>Harga</td>
                <td><input id="hrg" type="text" name="harga" value="<?=$har;?>"></td>
            </tr>

            <tr>
                <td width="130">Nama Penerima</td>
                <td><input id="np" type="text" name="nama_penerima" value= "<?= $user_nama;?>" ></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td><input id="almt" type="text" name="Alamat" value= "<?= $user_alamat;?>" ></td>
            </tr>
            <tr>
                <td>Nomor Telepon</td>
                <td><input id="nt" type="text" name="nomor_telepon" value= "<?= $user_nohp;?>" ></td>
            </tr>
            <tr>
                <td>Metode Pembayaran</td>
                <td><select id="mp" name="mpem">
                    <option value="tunai">Tunai</option>
                    <option value="non_tunai">Non Tunai</option></td>
            </tr>
            <tr>
                <td>Total Harga</td>
                <?php
                    if ($a == 1) {
                        echo "<td><input type='text' disabled='disabled' id='tot' name='totall' value='$atot'></td>";
                    }else{
                       echo "<td><input type='text' disabled='disabled' id='tot' name='totall'></td>";
                    }
                    
                ?>
            </tr>
            <tr>
                <td><input type="submit" name="beli" value="beli"/></td>
                
            </tr>

        </table>
    </form>
</body>
</html>
